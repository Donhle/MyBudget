<?php
/* Modèle des données de la table category*/

require_once ('lib/model/Model.php');

class Category extends Model
{
    // Retourne la liste des catégories de type recette
    public function findAllRecettes(): array {
        $result = $this->pdo->
        query("SELECT * FROM category
                        WHERE `transactionType` = '1'
                        ORDER BY `name` ASC");
        $listCatRecette = $result->fetchAll();

        return $listCatRecette;
    }

    // Retourne la liste des catégories de type dépense
    public function findAllDepenses(): array {
        $result = $this->pdo->
        query("SELECT * FROM category
                        WHERE `transactionType` = '-1'
                        ORDER BY `name` ASC");
        $listCatDepense = $result->fetchAll();

        return $listCatDepense;
    }

    // Retourne les noms de catégories
    public function findOnlyNames(): array {
        $result = $this->pdo->
        query("SELECT `categoryId`,`name` FROM category");
        $listCatName = $result->fetchAll(PDO::FETCH_KEY_PAIR);

        return $listCatName;
    }

    // Retourne le plus grand ID de la table
    public function getNb(){
        $result = $this->pdo->query("SELECT MAX(`categoryId`) AS MAX FROM `category`");
        $resultNb=$result->fetch();

        return $resultNb['MAX'];
    }
}