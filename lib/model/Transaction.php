<?php
/* Modèles des données de la table transaction */

require_once ('lib/model/Model.php');

class Transaction extends Model
{
    // ------------------ Créer, Modifier, Supprimer, Restaurer --------------------------------------------------------
    public function create($transacDateTime,$amount,$catId,$payId): string
    {
        $stmt=$this->pdo->
        prepare("INSERT INTO `transaction` 
            (`transactionDate`, `amount`, `idCategory`, `idPaymentmethod`)
            VALUES
            (?, ?, ?,?)");
        $stmt->execute(array($transacDateTime, $amount, $catId, $payId));
        if($stmt==true)
        {
            $result="OK";
        }
        else{
            $result=$stmt->errorCode();
        }
        return $result;
    }

    public function update($transacDateTime,$amount,$catId,$payId,$transacId): string
    {
        $stmt=$this->pdo->
        prepare("UPDATE `transaction` 
        SET `transactionDate`= ?,
        `amount`= ?,
        `idCategory`= ?,
        `idPaymentmethod`= ?,
        `dateSupp` = NULL
        WHERE `transactionId`= ?;");
        $stmt->execute(array($transacDateTime, $amount, $catId, $payId, $transacId));
        if($stmt==true)
        {
            $result="OK";
        }
        else{
            $result=$stmt->errorCode();
        }
        return $result;
    }

    public function delete($transacId): string
    {
        $Today=date("Y-m-d");
        $stmt=$this->pdo->
        prepare("UPDATE `transaction` 
        SET `dateSupp`='$Today'
        WHERE `transactionId`= ?");
        $stmt->execute(array($transacId));
        if($stmt==true)
        {
            $result="OK";
        }
        else{
            $result=$stmt->errorCode();
        }
        return $result;
    }

    public function restore($transacId): string
    {
        $stmt=$this->pdo->
        prepare("UPDATE `transaction`
        SET `dateSupp`= NULL
        WHERE `transactionId`= ?");
        $stmt->execute(array($transacId));
        if($stmt==true)
        {
            $result="OK";
        }
        else{
            $result=$stmt->errorCode();
        }
        return $result;
    }


    // --------------------- Retourne la liste des transactions demandées ---------------------------------------------
    public function getAskedTransaction($where): array {
        $query = $this->pdo->
        query("SELECT
        `transaction`.`transactionId`,
        `transaction`.`transactionDate`,
        `transaction`.`amount`,
        `transaction`.`dateSupp`,
        `category`.`name` AS `Catégorie`,
        `category`.`transactionType` AS `Type`,
        `payment_method`.`name` AS `Paiement`
        FROM `transaction`
        INNER JOIN `category` ON `transaction`.`idCategory` = `category`.`categoryId`
        INNER JOIN `payment_method` ON `transaction`.`idPaymentmethod` = `payment_method`.`payment_methodId`
        WHERE $where
        ORDER BY `transactionDate` ASC");

        $listTransactionAsked = $query->fetchAll();

        return $listTransactionAsked;
    }

    // --------------------- Retourne la liste des transactions demandées, en incluant les supprimées  -----------------
    public function getAskedTransactionWithDelete($where): array {
        $query = $this->pdo->
            query("SELECT 
            `transaction`.`amount`,
            `transaction`.`dateSupp`,
            `category`.`name` AS `Catégorie`,
            `category`.`transactionType` AS `Type`,
            `payment_method`.`name` AS `Paiement` 
            FROM `transaction`
            INNER JOIN `category` ON `transaction`.`idCategory` = `category`.`categoryId`
            INNER JOIN `payment_method` ON `transaction`.`idPaymentmethod` = `payment_method`.`payment_methodId`
            WHERE $where AND `transaction`.`dateSupp` IS NOT Null
            ORDER BY `transactionDate` ASC");

        $listTransactionAskedWithDelete = $query->fetchAll();

        return $listTransactionAskedWithDelete;
    }

    // ---------- Retourne la transaction qui doit être éditée ---------------------------------------------------------
    public function getTransactionToEdit($id): array {
        $query=$this->pdo->
        query("SELECT 
        `transaction`.`transactionDate`,
        `transaction`.`amount`,
        `category`.`transactionType`,
        `category`.`name` AS `Catégorie`, 
        `payment_method`.`name` AS `Paiement` 
        FROM `transaction`
        INNER JOIN `category` ON `transaction`.`idCategory` = `category`.`categoryId`
        INNER JOIN `payment_method` ON `transaction`.`idPaymentmethod` = `payment_method`.`payment_methodId`
        WHERE `transaction`.`transactionId`='$id'");

        $result=$query->fetch();

        return $result;
    }

    // ------------------------ Balance Dépense/Recette d'une liste de transactions ------------------------------------
    public function getAmountBalance($where): string {

        $resultR = $this->pdo->
            query("SELECT SUM(`amount`) AS R
            FROM `transaction`
            INNER JOIN `category` ON `transaction`.`idCategory` = `category`.`categoryId`
            WHERE $where AND `category`.`transactionType` = '1'");
        $resultRecetteAmount=$resultR->fetch();
        $r = $resultRecetteAmount['R'];

        $resultD = $this->pdo->
            query("SELECT SUM(`amount`) AS D
            FROM `transaction`
            INNER JOIN `category` ON `transaction`.`idCategory` = `category`.`categoryId`
            WHERE $where AND `category`.`transactionType` = '-1'");
        $resultDepenseAmount=$resultD->fetch();
        $d = $resultDepenseAmount['D'];

        $amountBalance = $r - $d;

        return round($amountBalance,2,PHP_ROUND_HALF_UP);
    }

    // ------------------------ Somme des recettes d'une liste de transactions ------------------------------------
    public function getSumRecettes($where) {

        $query = $this->pdo->
        query("SELECT ROUND(SUM(`amount`),2) AS R
            FROM `transaction`
            INNER JOIN `category` ON `transaction`.`idCategory` = `category`.`categoryId`
            WHERE $where AND `category`.`transactionType` = '1'");
        $result=$query->fetch();
        $r = $result['R'];

        return $r;
    }

    // ------------------------ Somme des dépenses d'une liste de transactions ------------------------------------
    public function getSumDepenses($where) {

        $query = $this->pdo->
        query("SELECT ROUND(SUM(`amount`),2) AS D
            FROM `transaction`
            INNER JOIN `category` ON `transaction`.`idCategory` = `category`.`categoryId`
            WHERE $where AND `category`.`transactionType` = '-1'");
        $result=$query->fetch();
        $d = $result['D'];

        return $d;
    }

    // ------------------------ Somme des dépenses groupées par catégories ------------------------------------
    public function getSumByCat($where) : array{

        $query = $this->pdo->
        query("SELECT 
       `category`.`name` as CatName,
       ROUND(SUM(`amount`),2) AS S
            FROM `transaction`
            INNER JOIN `category` ON `transaction`.`idCategory` = `category`.`categoryId`
            WHERE $where AND `category`.`transactionType` = '-1'
            GROUP BY `category`.`name`
            ORDER BY S DESC");
        $result=$query->fetchAll(PDO::FETCH_KEY_PAIR);

        return $result;
    }

    // ------------------------ Somme des dépenses groupées par moyen de paiement ------------------------------------
    public function getSumByPay($where) : array{

        $query = $this->pdo->
        query("SELECT 
       `payment_method`.`name` as PayName,
       ROUND(SUM(`amount`),2) AS S
            FROM `transaction`
            INNER JOIN `payment_method` ON `transaction`.`idPaymentmethod` = `payment_method`.`payment_methodId`
            INNER JOIN `category` ON `transaction`.`idCategory` = `category`.`categoryId`
            WHERE $where AND `category`.`transactionType` = '-1'
            GROUP BY `payment_method`.`name`
            ORDER BY S DESC");
        $result=$query->fetchAll(PDO::FETCH_KEY_PAIR);

        return $result;
    }

    // ------------ Retourne le plus grand ID de la table ------------------------------------------------------------
    public function getNb(){
        $query = $this->pdo->query("SELECT MAX(`transactionId`) AS MAX FROM `transaction`");
        $resultNb=$query->fetch();

        return $resultNb['MAX'];
    }

    // ------------------ Retourne les années contenant des transactions non supprimées ---------------------------------------------
    public function getYearAndNbOfAll(): array{
        $query = $this->pdo->
        query("SELECT YEAR(`transactionDate`) AS Y,
                        COUNT(`transactionId`) AS Count
                        FROM `transaction` 
                        WHERE `dateSupp` IS NULL 
                        GROUP BY Y");
        $result=$query->fetchAll(PDO::FETCH_KEY_PAIR);

        return $result;
    }
}