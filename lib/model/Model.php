<?php
    require_once ('lib/database.php');


    /* Pour chaque classe, on récupère $pdo */
class Model
{
    protected $pdo;

    public function __construct(){
        $this->pdo=getPdo();
    }

}