<?php
/* Modèle des données de la table des méthodes de paiement */
require_once ('lib/model/Model.php');

class PaymentMethod extends Model
{
    //Retourne la liste des méthodes de paiement
    public function findAll(): array {
        $result = $this->pdo->
        query("SELECT * FROM `payment_method` 
                ORDER BY `name` ASC");
        $listAllPayMethod = $result->fetchAll(PDO::FETCH_KEY_PAIR);

        return $listAllPayMethod;
    }

    //Retourne le plus grand ID de la table
    public function getNb(){
        $result = $this->pdo->query("SELECT MAX(`payment_methodId`) AS MAX FROM `payment_method`");
        $resultNb=$result->fetch();

        return $resultNb['MAX'];
    }
}