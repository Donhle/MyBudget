<?php 
/* Cette page permet de traiter la création/modification/suppression/restauration demandée à la page ModifierTransaction.php.
On y accède avec des paramètres POST.
A l'ouverture, une modal s'affiche pour informer l'utilisateur de la réussite ou l'échec de l'opération */

require_once("lib/Securite.php");
require_once("lib/ConvertDate.php");
require_once("lib/model/Transaction.php");
require_once("lib/utils.php");

//Instanciation
    $securite=new Securite();
    $convertDate=new ConvertDate();
    $transactionModel= new Transaction();

// On liste les paramètres "autorisés" pour le POST
$listAllowPost=array('transactionId',
    'amount',
    'categoryId',
    'MethodePaymentId',
    'transactionDate',
    'transactionTime',
    'CreerTransaction',
    'EditTransaction',
    'DeleteTransaction',
    'RestoreTransaction');

// On initialise les variables php qui stockeront le POST, ou false
$transactionId=false;
$amount=false;
$categoryId=false;
$MethodePaymentId=false;
$transactionDate=false;
$transactionTime=false;
$CreerTransaction=false;
$EditTransaction=false;
$DeleteTransaction=false;
$RestoreTransaction=false;

var_dump($amount);
/* On parcours le POST avec les paramètres autorisés.
*S'ils existent et sont non nuls, alors on les sécurise dans une variable PHP
 * S'ils n'existent pas, ils garderont la valeur false
 * */
foreach($listAllowPost as $value){
    if(array_key_exists($value,$_POST) && !empty($_POST[$value])){
        $$value=$securite->securite_BDD($_POST[$value]);
    }
}
var_dump($amount);

//On vérifie les paramètres POST reçus
//Si l'un des paramètres créer, ou edit, ou delete, ou restore sont différents de false, on continue la vérification
if($CreerTransaction!=false || $EditTransaction!=false || $DeleteTransaction!=false || $RestoreTransaction!=false){

    // S'il s'agit d'une création de transaction
    if($CreerTransaction == "'Creer'"){

        // On construit le date time pour MySQL
        $transactionDateTimeMySQL=$convertDate->createDateTime($transactionDate,$transactionTime);
        
        // Requête BDD d'insertion
        var_dump($amount);
        $transactionInsert=$transactionModel->create($transactionDateTimeMySQL,$amount,$categoryId,$MethodePaymentId);
        if($transactionInsert=="OK")
        {
            $success="La transaction a bien été créée";
        }
        else {
            $errorDetect=$transactionInsert;
        }
    }

    // S'il s'agit d'une modification
    else if($EditTransaction == "'Modifier'"){

        // On construit le date time pour MySQL
        $transactionDateTimeMySQL=$convertDate->createDateTime($transactionDate,$transactionTime);

        // Requête UPDATE
        $transactionUpdate=$transactionModel->update($transactionDateTimeMySQL,$amount,$categoryId,$MethodePaymentId,$transactionId);
        if($transactionUpdate=="OK")
        {
            $success="La transaction a bien été modifiée";
        }
        else {
            $errorDetect=$transactionUpdate;
        }
    }

    // S'il s'agit d'une "suppression" (== on renseigne la colonne dateSupp de la table transaction, pas de DELETE dans le shéma retenu)
    else if($DeleteTransaction == "'Supprimer'"){

        // Gestion de la suppression : Si on supprime, on ne prend pas en compte les éventuelles modifications de l'utilisateur. 
        $transactionDelete=$transactionModel->delete($transactionId);
        if($transactionDelete=="OK")
        {
            $success="La transaction a bien été supprimée";
            $successDelete=true;
        }
        else {
            $errorDetect=$transactionDelete;
        }
    }

    // S'il s'agit d'une restauration
    else if($RestoreTransaction == "'Restaurer'"){

        // Requête UPDATE
        $transactionUpdate=$transactionModel->restore($transactionId);
        if($transactionUpdate=="OK")
        {
            $success="La transaction a bien été restaurée";
        }
        else {
            $errorDetect=$transactionUpdate;
        }
    }
    // Si les paramètres reçus sont altérés ou non valides, on ne poursuit pas
    else {redirect_404();}
}
//Si aucun des paramètres créer, édit, delete ou restaure n'est différent de false, alors on redirige vers le 404
else{redirect_404();}