<?php
/* Cette classe calcule les dates "par défaut" utilisées par l'application */

class DefaultDate
{
    // Retourne le premier jour du mois en cours au format "YYYY-MM-DD"
    public function actualMonthBegin() {
        $monthBegin=date
        ('Y-m-d',
            mktime(0,0,0,
                date("m"),1,date("Y"))
        );

        return $monthBegin;
    }

    // Retourne le dernier jour du mois en cours au format "YYYY-MM-DD"
    public function actualMonthEnd() {
        $monthEnd=date
        ('Y-m-d',
            mktime(23,59,59,
                date("m")+1,0,date("Y"))
        );
        return $monthEnd;
    }

    // Retourne le dernier jour de l'année en cours au format "YYYY-MM-DD"
    public function actualYearEnd() {
        $yearEnd=date
        ('Y-m-d',
            mktime(23,59,59,
                12,31,date("Y"))
        );
        return $yearEnd;
    }

    // Retourne le premier jour de l'année en cours au format "YYYY-MM-DD"
    public function actualYearBegin() {
        $yearBegin= date
        ('Y-m-d',
            mktime(0,0,0,
                1,1,date("Y"))
        );

        return $yearBegin;
    }

    // Retourne le premier jour d'une année (paramètre) au format "YYYY-MM-DD"
    public function getYearBeginOfYear($y) {
        $yearBegin= date
        ('Y-m-d',
            mktime(0,0,0,
                1,1,$y)
        );

        return $yearBegin;
    }

    // Retourne le dernier jour d'une année (paramètre) au format "YYYY-MM-DD"
    public function getYearEndOfYear($y) {
        $yearEnd= date
        ('Y-m-d',
            mktime(23,59,59,
                12,31,$y)
        );

        return $yearEnd;
    }


}