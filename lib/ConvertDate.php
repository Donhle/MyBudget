<?php

/* Cette classe permet de convertir des dates dans les formats demandés par les différents traitements */

 class ConvertDate
{
    //Convertir une date "Y-m-d" au format "Y-m-d H:i:s"
    public function DateToMysql($d,$p) {
        //Si $p = "d" => L'heure du datetime sera le jour demandé à minuit
        if($p=="d"){
            $dateMySQL = date
            ('Y-m-d H:i:s',
                (mktime
                (0, 0, 0,
                    substr($d,6,2),
                    substr($d,9,2),
                    substr($d,1,4))));

            return $dateMySQL;
        }
        //Si $p = "d" => L'heure du datetime sera le jour demandé à 23:59:59
        else if ($p=="e"){
            $dateMySQL = date
            ('Y-m-d H:i:s',
                (mktime
                (23, 59, 59,
                    substr($d,6,2),
                    substr($d,9,2),
                    substr($d,1,4))));

            return $dateMySQL;
        }
        }


    //Assembler une date "YYYY-MM-DD" et une heure "HH:MM:SS" en "YYY-MM-DD HH:MM:SS"
     public function createDateTime($d,$t){
        $dateMySQL= date(
            'Y-m-d H:i:s',
            mktime
            (substr($t,1,2),
                substr($t,4,2),
                0,
                substr($d,6,2),
                substr($d,9,2),
                substr($d,1,4)));

        return $dateMySQL;
     }

    // Extraire uniquement la date d'un format "AAA-MM-DD HH:MM:SS"
     public function extractDate($d) {
        $dateOnly =
            substr($d,0,10);

        return $dateOnly;
     }

     // Extraire uniquement l'heure d'un format "AAA-MM-DD HH:MM:SS"
     public function extractTime($d) {
         $timeOnly =
             substr($d,11,8);

         return $timeOnly;
     }

     // Convertir une date "AAAA-MM-DD HH:MM:SS" en "jj/mm/aaaa à hh:mm" (Affichage Front)
     public function dateUserFriendly($d) {
        $dateUserFriendly = date("j/n/Y \à H\hi",
            (mktime
                (substr($d,11,2),
                substr($d,14,2),
                0,
                substr($d,5,2),
                substr($d,8,2),
                substr($d,0,4))));

        return $dateUserFriendly;
     }

}