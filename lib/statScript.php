<?php 
/* Cette page permet d'afficher un rapport annuel.
Par défaut, le rapport se calcule sur l'année en cours.
Une modal permet à l'utilisateur de choisir une autre année. S'il le fait, on recharge cette page avec un paramètre POST */

require_once("lib/DefaultDate.php");
require_once("lib/model/Transaction.php");
require_once("lib/utils.php");

// Instanciation des classes :
    //1er et dernier jour d'une année
    $defaultDateModel=new DefaultDate();

    //Transactions
    $transacModel = new Transaction();

/* On initialise la variable Année qui servira aux données du rapport
* Par défaut, on analyse l'année en  cours
*/
$yearToAnalyze=date('Y');

/* On regarde si il existe un paramètre 'year' dans le POST.
S'il y en a un, c'est qu'il faut analyser une autre année que celle en cours. On sécurise le paramètre reçu.
* On remplace l'année en cours par le paramètre reçu
*/
if (array_key_exists('year',$_POST) && !empty($_POST['year']) && is_numeric($_POST['year'])){
    $yearToAnalyze=intval($_POST['year']);
    //Si POST contient un paramètre year est qu'il n'est pas numérique, alors on redirige vers le 404
} else if(array_key_exists('year',$_POST) && !empty($_POST['year']) && !is_numeric($_POST['year']))
{redirect_404();}

// On détermine le 1er et le dernier jour de l'année à analyser
$firstDayOfYear = $defaultDateModel->getYearBeginOfYear($yearToAnalyze);
$lastDayOfYear = $defaultDateModel->getYearEndOfYear($yearToAnalyze);

// Initialisation du WHERE de la requête SQL (par défaut on requête tout sauf les transactions supprimées)
$SQLWhere = "`transaction`.`dateSupp` IS NULL
              AND `transaction`.`transactionDate` >= '$firstDayOfYear' 
              AND `transaction`.`transactionDate`<= '$lastDayOfYear'";

//On compte le nombre de transactions dans la période demandée
$nbTransac=count($transacModel->getAskedTransaction($SQLWhere));


// Graphique 1 : Balance Dépenses/Recettes d'une année donnée

    //Recettes
    $amountRecettes=$transacModel->getSumRecettes($SQLWhere);

    //Dépenses
    $amountDepenses=$transacModel->getSumDepenses($SQLWhere);

    //Balance
    $amountBalance=round($amountRecettes-$amountDepenses,2,PHP_ROUND_HALF_UP);


// Graphique 2 : Répartition des dépenses par moyen de paiement

    $listSumAmountByPay=$transacModel->getSumByPay($SQLWhere);
    $nbLignesSumAmountByPay=count($listSumAmountByPay);


// Graphique 3 : Répartition des dépenses par catégories

    $listSumAmountByCat=$transacModel->getSumByCat($SQLWhere);
    $nbLignesSumAmountByCat=count($listSumAmountByCat);