<?php
/* Cette page permet de créer, modifier, supprimer, restaurer une transaction.
On y accède depuis List.php, avec des paramètres GET*/

require_once("lib/utils.php");
require_once("lib/model/Transaction.php");
require_once("lib/ConvertDate.php");
require_once("lib/model/Category.php");
require_once("lib/model/PaymentMethod.php");

date_default_timezone_set('Europe/Paris'); //pour la récupération de l'heure actuelle dans le bon fuseau horaire


// Instanciation des classes :
    //Requête transaction BDD
    $transactionModel=new Transaction();

    //Convertir date/heure de MySQL vers front
    $convertDate= new ConvertDate();

    //Liste des catégories et des moyens de payements
    $categoryModel=new Category();
    $payModel=new PaymentMethod();

// On traite le GET et on le sécurise
if(array_key_exists('idTransaction',$_GET) && is_numeric($_GET['idTransaction'])){
    $idTransaction=intval($_GET['idTransaction']);

    /* Si le paramètre idTransaction est plus grand que le plus grand id de la table,
    alors paramètre modifié par user  -> 404
    */
    if($idTransaction>$transactionModel->getNb()){redirect_404();}

    // S'il est supérieur à 0, mais inférieur au plus grand id de la table, c'est une modfication de transaction
     else if($idTransaction > 0 && $idTransaction<=$transactionModel->getNb()){

             // On vérifie qu'il ne s'agit pas d'une transaction à restaurer, dans ce cas il s'agit bien d'une modification
             if(!array_key_exists('Restore',$_GET)){
                 $pageTitle="Modification d'une transaction";
                 $op="M";}
             //Sinon c'est une restauration
             else if((array_key_exists('Restore',$_GET)) && (is_numeric($_GET['Restore'])) && (intval($_GET['Restore'])==1)){
                 $pageTitle="Restaurer une transaction";
                 $op="R";}
             else {redirect_404();} // Prévention modifications user du GET

             // On récupère la transaction dans la BDD
             $transactionToDisplay=$transactionModel->getTransactionToEdit($idTransaction);

             //On sépare date et heure (passage du format MySQL au format date + heure pour affichage dans les champs dédiés)

             $transactionDateToDisplay=$convertDate->extractDate($transactionToDisplay['transactionDate']);
             $transactionTimeToDisplay=$convertDate->extractTime($transactionToDisplay['transactionDate']);
         }

    // S'il est égal à 0, c'est une création de transaction
    else if($idTransaction == 0){
        
        $pageTitle="Création d'une transaction";
        $op="C";

        $actualDate=date("Y-m-d");
        $actualTime=date("H:i:s");
    }
}
// S'il n'y a pas de paramètre idTransaction, ou un paramètre non int -> 404
else {redirect_404();}


// On récupère la liste des catégories et des modes de paiement en BDD pour compléter le menu déroulant des filtres associés

$listeCategory=$categoryModel->findOnlyNames();
$listePaymentMethod=$payModel->findAll();