<?php
require_once("lib/utils.php");

// Numéro de version
$version = getVersionAppli();
?>

<footer class="page-footer blue">
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      <span class="right"><?php echo $version ?></span>
      </div>
    </div>
</footer>