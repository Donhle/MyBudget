<?php
/* Cette page contient la liste des transactions.
Le script php du début récupère les éventuels paramètres envoyés en GET, qui servent à filtrer les données affichées.
Les filtres sont définis à l'aide d'une modal.
*/

require_once("lib/Securite.php");
require_once("lib/DefaultDate.php");
require_once("lib/ConvertDate.php");
require_once("lib/model/Category.php");
require_once("lib/model/PaymentMethod.php");
require_once("lib/model/Transaction.php");
require_once("lib/utils.php");

// Instanciation des classes :
    // Securisation des variables GET
    $securite = new Securite();

    // Convertir des dates
    $convert_Date= new ConvertDate();

    // Liste des catégories et des moyens de paiements de la BDD
    $catModel = new Category();
    $payModel = new PaymentMethod();

    $catName = $catModel->findOnlyNames();
    $payMethod = $payModel->findAll();

    // Dates pré-calculées pour les filtres
    $defaultDate = new DefaultDate();
    $monthBegin = $defaultDate->actualMonthBegin();
    $monthEnd = $defaultDate->actualMonthEnd();
    $yearBegin = $defaultDate->actualYearBegin();
    $yearEnd = $defaultDate->actualYearEnd();

    // Liste des transactions
    $transactionModel = new Transaction();


// On liste les paramètres "autorisés" pour le GET
$listAllowGet=array('amountMin',
    'amountMax',
    'categoryId',
    'MethodePaymentId',
    'isDelete',
    'transactionDateMin',
    'transactionDateMax',
    'filtre');

// On initialise les variables php qui stockeront le GET, ou false
$amountMin=false;
$amountMax=false;
$categoryId=false;
$MethodePaymentId=false;
$isDelete=false;
$transactionDateMin=false;
$transactionDateMax=false;
$filtre=false;

/* On parcours le GET avec les paramètres autorisés.
*S'ils existent et sont non nuls, alors on les sécurise dans une variable PHP
 * S'ils n'existent pas, ils garderont la valeur false
 * */

foreach ($listAllowGet as $value){
    if(isset($_GET[$value]) && !empty($_GET[$value])){
        $$value=$securite->securite_BDD($_GET[$value]);
    }
}

// On construit le date time pour MySQL (si le champ existe)
if($transactionDateMin!=false){
    $DateMinMySQL = $convert_Date->DateToMysql($transactionDateMin,"d");
}
if($transactionDateMax!=false){
    $DateMaxMySQL = $convert_Date->DateToMysql($transactionDateMax,"e");
}

// On détermine les champs qui ont été remplis par l'utilisateur, et on construit la clause WHERE de la requête ($SQLWhere)
// On costruit en même temps la chaîne de texte qui récapitule les filtres choisis ($afficheFiltre)
$SQLWhere = '';
$nbFiltre = '0';
$afficheFiltre= '<a href="List.php">Réinitialiser les filtres</a><br><br>Affichage des opérations';

    //A chaque passage, on incrémente $nbFiltre pour déterminer s'il faut rajouter "AND" dans la requête SQL et <br> dans l'affichage du filtre utilisateur
    // Si c'est le 1er filtre sélectionné, alors $nbFiltre =0, et donc ni AND ni <br> ne sont nécessaires.

    if($amountMin!=false){
        if ($nbFiltre>0){$SQLWhere.=" AND ";$afficheFiltre.=",<br>";}
        $SQLWhere = "`amount` >= '$amountMin'";
        $afficheFiltre.=" dont le montant est supérieur à ".$amountMin;
        $nbFiltre ++;
    }
    if($amountMax!=false){
        if ($nbFiltre>0){$SQLWhere .=" AND ";$afficheFiltre.=",<br>";}
        $SQLWhere .= "`amount` <= '$amountMax'";
        $afficheFiltre.=" dont le montant est inférieur à ".$amountMax;
        $nbFiltre ++;
    }
    if(isset($DateMinMySQL)){
        if ($nbFiltre>0){$SQLWhere .=" AND ";$afficheFiltre.=",<br>";}
        $SQLWhere .= "`transactionDate` >= '$DateMinMySQL'";
        //On convertit la date du format MySQL au format "d/m/Y hh:mm:ss" pour l'affichage utilisateur
        $afficheFiltre.=" ayant eu lieu après le ".$convert_Date->dateUserFriendly($DateMinMySQL);
        $nbFiltre ++;
    }
    if(isset($DateMaxMySQL)){
        if ($nbFiltre>0){$SQLWhere .= " AND ";$afficheFiltre.=",<br>";}
        $SQLWhere .= "`transactionDate` <= '$DateMaxMySQL'";
        //On convertit la date du format MySQL au format "d/m/Y hh:mm:ss" pour l'affichage utilisateur
        $afficheFiltre.=" ayant eu lieu avant le ".$convert_Date->dateUserFriendly($DateMaxMySQL);
        $nbFiltre ++;
    }
    if($categoryId!=false){
        // Si l'id de category est supérieur au nb d'id de la table category, alors on redirige vers le 404
        if($categoryId>$catModel->getNb()){redirect_404();}
        if ($nbFiltre>0){$SQLWhere .= " AND ";$afficheFiltre.=",<br>";}
        $SQLWhere .= "`idCategory` = '$categoryId'";
        $afficheFiltre.=" de la catégorie ".$catName[$categoryId];
        $nbFiltre ++;
    }
    if($MethodePaymentId!=false){
        // Si l'id de paiement est supérieur au nb d'id de la table payment_method, alors on redirige vers le 404
        if($MethodePaymentId>$payModel->getNb()){redirect_404();}
        if ($nbFiltre>0){$SQLWhere .= " AND ";$afficheFiltre.=",<br>";}
        $SQLWhere .= "`idPaymentmethod` = '$MethodePaymentId'";
        $afficheFiltre.=" payé par ".$payMethod[$MethodePaymentId];
        $nbFiltre ++;
    }
    // Si $isDelete == false, alors il ne faut pas afficher les transactions "supprimées" (==avec une date dans la colonne dateSupp)
    if($isDelete==false){
        if ($nbFiltre>0){$SQLWhere .= " AND ";}
        $SQLWhere .="`dateSupp` IS NULL";
        $nbFiltre ++;
    } else if ($isDelete==1){
        if ($nbFiltre>0){$afficheFiltre.=",<br>";}
        $afficheFiltre.=" incluant les opérations supprimées";
    }
    // Si isDelete a une autre valeur que 1 ou false, alors on redirige vers le 404
    else if ($isDelete!=1 && $isDelete!=false){redirect_404();}

    // Si aucun filtre n'est sélectionné par l'utilisateur, alors $SLQWhere = 1 (pour tout requêter)
    if(empty($SQLWhere)){$SQLWhere = 1;}

// S'il s'agit d'un filtre pré-paramétré ($_GET['filtre']=X), on modifie le $SQLWhere en conséquence
    if($filtre!=false) {
        $afficheFiltre = '<a href="List.php">Réinitialiser les filtres</a><br>';
        if ($filtre == 1) {  // Afficher uniquement les transactions supprimées
            $SQLWhere = "`dateSupp` IS NOT NULL";
            $afficheFiltre .= "Affichage des opérations supprimées";
        }
        if ($filtre == 2) {  // Afficher transactions de l'année en cours
            $SQLWhere = "`transactionDate`>= '$yearBegin' AND `transactionDate`<= '$yearEnd' AND `dateSupp` IS NULL";
            $afficheFiltre .= "Affichage des opérations de l'année en cours";
        }
        if ($filtre == 3) { // Afficher uniquement les recettes
            $SQLWhere = "`category`.`transactionType`='1' AND `dateSupp` IS NULL";
            $afficheFiltre .= "Affichage des recettes";
        }
        if ($filtre == 4) { // Afficher uniquement les dépenses
            $SQLWhere = "`category`.`transactionType`='-1' AND `dateSupp` IS NULL";
            $afficheFiltre .= "Affichage des dépenses";
        }
        //Si le GET inclue un paramètre $filtre supérieur au nombre de filtre existant (4), alors on redirige vers le 404
        if ($filtre > 4) {{redirect_404();}}
    }


// Requête en BDD : Liste des transactions en tenant compte du filtre si rempli

$listTransactionAsked = $transactionModel -> getAskedTransaction($SQLWhere);

//On compte le nombre de lignes que contient le tableau affiché
$nbLignesPhp=count($listTransactionAsked);

//Requête Nb de lignes supprimées affichées (si l'utilisateur l'a demandé)
if($isDelete==1){
$listTransactionAskedWithDelete = $transactionModel ->getAskedTransactionWithDelete($SQLWhere);
    //On compte le nombre de lignes que contient le tableau affiché
    $nbLignesPhpSup=count($listTransactionAskedWithDelete);
}

// Somme des montants des lignes affichées (balance recettes - dépenses)

$amountBalance=$transactionModel->getAmountBalance($SQLWhere);
?>