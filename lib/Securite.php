<?php
require_once("database.php");

/* Cette classe permet de sécuriser des données GET ou POST reçues (prévention des injections SQL)*/

class Securite
{
    public function securite_BDD($string){

        $pdo=getPdo();
        $floatPointCheck=strpos($string,".",0);
        if($floatPointCheck==true){$string=floatval($string);}
        else if(ctype_digit($string)) {// On regarde si le type de $string est un nombre entier (int)
            $string = intval($string);
        } 
        else { // Pour tous les autres types
            $string = $pdo->quote($string);
                }
        return $string;
    }
//fin securité

    public function random_pw($pw_length) {
        $pass = NULL;
        $charlist = 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz023456789';
        $ps_len = strlen($charlist);
        mt_srand((double)microtime()*1000000);

        for($i = 0; $i < $pw_length; $i++) {
            $pass .= $charlist[mt_rand(0, $ps_len - 1)];
        }
        return ($pass);
    }
}