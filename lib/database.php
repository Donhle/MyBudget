<?php
/* Ce fichier permet la connexion à la base de données, utilisée dans la classe MODEL.PHP */

// Environnement de développement
function getPdo():PDO
{
    $pdo=new PDO(
        'mysql:host=localhost;dbname=budgetrendu;charset=utf8',
        'root','',
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, // ==> Si erreur, affichage des messages erreurs, désactivé en production
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC] // Quand il y a requête, on retourne tableau associatif auto
);
    return $pdo;
}
?>

