<?php
require_once('lib/statScript.php');
?>
<!-- ////////////////////////////////////////////////////// HTML ///////////////////////////////////////////// -->
<!doctype html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Bilan annuel</title>
    <!-- CSS  du framework materialize -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="CSS/materialize.css" type="text/css" rel="stylesheet" media="screen"/>
    <!-- Surcouche CSS -->
    <link href="CSS/style.css" type="text/css" rel="stylesheet" media="screen"/>
    <!-- JQuery DatePicker -->
    <link rel="stylesheet" href="jQuery/jquery-ui.min.css">
    <!-- CSS chartJS -->
    <link href="CSS/ChartJS-2.9.4.min.css" type="text/css" rel="stylesheet">
    <!-- Script ChartJS -->
    <script src="JS/ChartJS-2.9.4-budle.min.js"></script>
</head>

<body class="grey lighten-2">
<nav class="light-blue lighten-1">
    <div class="nav-wrapper container">
        <a id="logo-container" href="index.php" class="brand-logo">
            <img src="img/LogoMyBudget.png" id="LogoAppli" alt="Logo de l'application MyBudget">
        </a>
        <h1 class="center appli-name hide-on-med-and-down">MyBudget</h1>

        <!-- //////////////////////////  Menu Mobile ////////////////////  -->
        <ul id="nav-mobile" class="sidenav blue lighten-4">
            <li><a href="List.php">Liste Opérations</a></li>
            <li><a href="#"><b>Bilan annuel</b></a></li>
            <li><a href="index.php">Quitter</a></li>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
    <a href="List.php" class="btn purple lighten-1 right RapportListeButton hide-on-med-and-down">Liste</a>
    <a href="index.php" class="btn red right QuitterButton">Quitter</a>
</nav>

<!-- /////////////  Page ///////////////////////////////////////// -->
<div class= "container">
    <div class="row">
        <h2 class="center noMarginTop noMarginBottom">Bilan annuel <?php echo $yearToAnalyze; ?></h2>
        <a href="#DefineYear"  class="waves-effect waves-light btn modal-trigger orange lighten-2 right">Sélection année</a>
    </div>
    <!-- //////////////////////////////////////////  Modal Sélection filtres ///////////////////////////////////////-->
    <div id="DefineYear" class="modal fixWidthModal">
        <div class="modal-content">
            <form class="col s12" action="Stat.php" method="post">
                <!-- Liste des années contenant au moins une transaction non supprimée -->
                <div class="input-field col s12 l6">
                    <?php
                    //On liste les années qui contiennent au moins 1 transaction non supprimée
                    $yearToDisplay = $transacModel -> getYearAndNbOfAll();
                    foreach ($yearToDisplay as $year => $count) {
                        echo "<p><label><input name ='year' type='radio' class='with-gap' value='$year' /><span>".$year."</span></label></p>";
                    }
                    ?>
                </div>
                <button class="btn">Envoyer</button>
            </form>
        </div>
    </div>
    <!-- Récapitulatif Nb de transactions, sommes des revenus et des dépenses, balance annuelle -->
    <div class="row">
        <div class="col s12 l12 center">
            <h3>Informations</h3>
            <p><?php echo $nbTransac; ?> transactions</p>
            <p><?php echo $amountRecettes; ?> € de revenus.</p>
            <p><?php echo $amountDepenses; ?> € de dépenses.</p>
            <p class="<?php 
            // Si la balance annuelle est positive, elle s'affiche en vert, sinon en rouge
            if($amountBalance < 0){echo"red";}else{echo"green";}?>-text">Balance Recettes/Dépenses: <?php echo $amountBalance; ?> €
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m6 l6">
            <!-- Graphique 1 : camembert de répartition Dépenses/Revenus -->
            <canvas class="chart-container" id="chartAnnualBalance"></canvas>
            <script>
                var ctx = document.getElementById("chartAnnualBalance").getContext("2d");
                var myChart = new Chart(ctx, {
                    type: "pie",
                    data: {
                        labels: ["Dépenses", "Revenus"],
                        datasets: [{
                            label: "Répartition Dépenses / Revenus",
                            data: [<?php echo $amountDepenses.",".$amountRecettes?>],
                            backgroundColor: [
                                "rgba(255, 99, 132, 0.2)",
                                "rgba(54, 162, 235, 0.2)"
                            ],
                            borderColor: [
                                "rgba(255, 99, 132, 1)",
                                "rgba(54, 162, 235, 1)"
                            ],
                            borderWidth: 3
                        }]
                    },
                    options:{
                        title:{
                            display:true,
                            text: 'Balance Recettes/Dépenses'
                        }
                    }
                });
            </script>
        </div>
        <!-- Graphique 2 : camembert dépenses par moyen de paiement -->
        <div class="col s12 m6 l6">
            <canvas class="chart-container" id="DepensesByPayMethod"></canvas>
            <script>
                var ctx = document.getElementById("DepensesByPayMethod").getContext("2d");
                var myChart = new Chart(ctx, {
                    type: "doughnut",
                    data: {
                        labels: [
                            <?php
                            $nbRows = $nbLignesSumAmountByPay;
                            foreach ($listSumAmountByPay as $payName => $value){
                                $nbRows --;
                                if($nbRows>0){echo "'".$payName."', ";}
                                else {echo "'".$payName."'";}
                            }
                            ?>
                        ],
                        datasets: [{
                            label: "Somme",
                            data: [
                                <?php
                                $nbRows = $nbLignesSumAmountByPay;
                                foreach ($listSumAmountByPay as $payName => $value){
                                    $nbRows--;
                                    if($nbRows>0){echo $value." ,";}
                                    else {echo $value;}
                                }
                                ?>
                            ],
                            backgroundColor: [
                                "rgba(0, 0, 255, 0.2)",
                                "rgba(255, 255, 0, 0.2)",
                                "rgba(255, 0, 0, 0.2)",
                                "rgba(255, 0, 255, 0.2)"
                            ],
                            borderColor: [
                                "rgba(0, 0, 255, 1)",
                                "rgba(255, 255, 0, 1)",
                                "rgba(255, 0, 0, 1)",
                                "rgba(255, 0, 255, 1)"
                            ],
                            borderWidth: 5
                        }]
                    },
                    options:{
                        title:{
                            display:true,
                            text: 'Dépenses par moyen de paiement'
                        }
                    }
                });
            </script>
        </div>

    </div>
    <div class="row">
        <!-- Graphique 3 : histogramme dépenses décroissantes par catégories  -->
        <div class="col l12">
            <canvas class="chart-container" id="DepensesByCategory"></canvas>
            <script>
                var ctx = document.getElementById("DepensesByCategory").getContext("2d");
                var myChart = new Chart(ctx, {
                    type: "bar",
                    data: {
                        labels: [
                            <?php
                            $nbRows = $nbLignesSumAmountByCat;
                            foreach ($listSumAmountByCat as $catName => $value){
                                $nbRows --;
                                if($nbRows>0){echo "'".$catName."', ";}
                                else {echo "'".$catName."'";}
                            }
                            ?>
                                ],
                        datasets: [{
                            label: "Somme",
                            data: [
                                <?php
                                $nbRows = $nbLignesSumAmountByCat;
                                foreach ($listSumAmountByCat as $catName => $value){
                                    $nbRows--;
                                    if($nbRows>0){echo $value." ,";}
                                    else {echo $value;}
                                    }
                                ?>
                                    ],
                            backgroundColor: [
                                "rgba(192, 192, 192, 0.2)",
                                "rgba(128, 128, 128, 0.2)",
                                "rgba(0, 0, 0, 0.2)",
                                "rgba(255, 0, 0, 0.2)",
                                "rgba(128, 0, 0, 0.2)",
                                "rgba(255, 255, 0, 0.2)",
                                "rgba(128, 128, 0, 0.2)",
                                "rgba(0, 255, 0, 0.2)",
                                "rgba(0, 128, 0, 0.2)",
                                "rgba(0, 255, 255, 0.2)",
                                "rgba(0, 128, 128, 0.2)",
                                "rgba(0, 0, 255, 0.2)",
                                "rgba(0, 0, 128, 0.2)",
                                "rgba(255, 0, 255, 0.2)",
                                "rgba(128, 0, 128, 0.2)",
                                "rgba(60, 0, 0, 0.2)",
                                "rgba(0, 60, 0, 0.2)",
                                "rgba(0, 0, 60, 0.2)",
                                "rgba(60, 0, 60, 0.2)",
                                "rgba(60, 60, 0, 0.2)",
                                "rgba(0, 60, 60, 0.2)",
                                "rgba(60, 0, 255, 0.2)"
                            ],
                            borderColor: [
                                "rgba(192, 192, 192, 1)",
                                "rgba(128, 128, 128, 1)",
                                "rgba(0, 0, 0, 1)",
                                "rgba(255, 0, 0, 1)",
                                "rgba(128, 0, 0, 1)",
                                "rgba(255, 255, 0, 1)",
                                "rgba(128, 128, 0, 1)",
                                "rgba(0, 255, 0, 1)",
                                "rgba(0, 128, 0, 1)",
                                "rgba(0, 255, 255, 1)",
                                "rgba(0, 128, 128, 1)",
                                "rgba(0, 0, 255, 1)",
                                "rgba(0, 0, 128, 1)",
                                "rgba(255, 0, 255, 1)",
                                "rgba(128, 0, 128, 1)",
                                "rgba(60, 0, 0, 1)",
                                "rgba(0, 60, 0, 1)",
                                "rgba(0, 0, 60, 1)",
                                "rgba(60, 0, 60, 1)",
                                "rgba(60, 60, 0, 1)",
                                "rgba(0, 60, 60, 1)",
                                "rgba(60, 0, 255, 1)"
                            ],
                            borderWidth: 5
                        }]
                    },
                    options:{
                        title:{
                            display:true,
                                text: 'Dépenses par catégorie'
                        },
                        legend:{
                            display:false
                        }
                    }
                });
            </script>
        </div>
    </div>
    <div class="row withMarginBottom"></div>
</div>
<!-- ////////////////////////////////////////// Footer ////////////////////////////////////////////////////////// -->
<?php
include('lib/templates/footer.php');
?>

<!--  Scripts liés au framework Materialize + jQuey -->
<script src="jQuery/jquery-2.1.1.min.js"></script>
<script src="jQuery/jquery-ui.min.js"></script>
<script src="JS/materialize.js"></script>
<!-- Script paramétrage jQuery -->
<script src="JS/init.js"></script>

</body>
</html>
