<?php
require_once('lib/updateTransactionScript.php');

?>
<!-- ////////////////////////////////////// PAGE HTML ////////////////////////////////////////////////////////////// -->
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $pageTitle ?></title>
    <!-- CSS  du framework materialize -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="CSS/materialize.css" type="text/css" rel="stylesheet" media="screen"/>
    <!-- Surcouche CSS -->
    <link href="CSS/style.css" type="text/css" rel="stylesheet" media="screen"/>
    <!-- JQuery -->
    <link rel="stylesheet" href="jQuery/jquery-ui.min.css">
</head>

<body class="grey lighten-2">
<nav class="light-blue lighten-1">
    <div class="nav-wrapper container">
        <a id="logo-container" href="#" class="brand-logo">
            <img src="img/LogoMyBudget.png" id="LogoAppli" alt="Logo de l'application MyBudget">
        </a>
      <h1 class="center appli-name hide-on-med-and-down">MyBudget</h1>
    </div>
</nav>
<!-- /////////////////////////////////////////////// Contenu page ////////////////////////////////////////////////// -->
<div class="container">
    <div class="row">
        <h2 class="center noMarginTop noMarginBottom"><?php echo $pageTitle ?></h2>
    </div>
<!-- Bouton retour -->
    <div class="row noMarginBottom"><a href="List.php" class="btn blue left"><i class="tiny material-icons">chevron_left</i></a></div>
    
<!-- Form création/modification de la transaction -->
    
    <!-- Pour chaque champs : S'il s'agit d'une modification de transaction, 
    alors les valeurs stockées en BDD sont affichées ($transactionToDisplay), sinon vide (à part la date / heure)
     S'il s'agit d'une restauration de transaction, tous les input sont disabled -->
    
    <form class="col s12" action="ModifierTransactionMaJ.php" method="post">	
        <?php if($op=="M" || $op=="R"){ // Si c'est une modification ou restauration de transaction, alors l'ID est obligatoire et sera transmis (hidden)
            echo '<input type="number" class="hide" name="transactionId" value='.$idTransaction.' required>';}
            ?>
        <!-- Champ Montant -->
        <div class="row noMarginBottom">
            <div class="input-field col s12 l6">
                <input type="number" class="champForm validate" name="amount" min="0" step="0.01" id="amount"
                value="<?php if(isset($transactionToDisplay)){echo $transactionToDisplay['amount'];}?>"
                    <?php if($op=="R"){echo " disabled";} ?>
                       required>
                <label for="amount">Montant (€)</label>
            </div>
        </div>
        <!-- Champ Date : Soit la date stockée en BDD, soit la date actuelle (pour la création)-->
        <div class="row">
            <div class="input-field col s12 l6">
                <input type="text" class="champForm datepicker" name="transactionDate" id="dateTransaction"
                value="<?php if(isset($transactionToDisplay)){echo "$transactionDateToDisplay";} else if(isset($actualDate)){echo $actualDate;}?>"
                    <?php if($op=="R"){echo " disabled";} ?>
                       required>
                <label for="dateTransaction">Date</label>
            </div>
            <!-- Champ Heure :Soit l'heure stockée en BDD, soit l'heure actuelle (pour la création)-->
            <div class="input-field col s12 l6">
                <input type="text" class="champForm timepicker" name="transactionTime" id="heureTransaction"
                value="<?php if(isset($transactionToDisplay)){echo "$transactionTimeToDisplay";} else if(isset($actualTime)){echo $actualTime;}?>"
                    <?php if($op=="R"){echo " disabled";} ?>
                       required>
                <label for="heureTransaction">Heure</label>
            </div>
        </div>
        <!-- Menus déroulant Catégories et moyen de paiement -->
        <div class="row">
            <div class="input-field col s12 l6">
                <select size="1" class="champFormSelect" name="categoryId"
                        <?php if($op=="R"){echo " disabled";} ?> required>
                    <option value="">-- Choix --</option>
                    <?php
                        // On récupère la liste des catégories de la BDD
                        
                        foreach ($listeCategory as $keyIdCategory => $valueCategory) {
                        /* Si c'est une modification ou restauration de transaction, alors on sélectionne
                        par défaut la catégorie stockée en BDD pour cette transaction */
                            if($op=="M" || $op=="R"){
                                if($valueCategory==$transactionToDisplay['Catégorie']){
                                    echo "<option value=\"$keyIdCategory\" selected>$valueCategory</option>";
                                }
                                else {
                                    echo "<option value=\"$keyIdCategory\">$valueCategory</option>";
                                };}
                        // Si on créé une transaction, alors aucune catégorie n'est présélectionnée
                            else {echo "<option value=\"$keyIdCategory\">$valueCategory</option>";}
                        }
                    ?>
                </select>
                <label>Catégorie</label>
            </div>
            <div class="input-field col s12 l6">
                <select size="1" class="champFormSelect" name="MethodePaymentId"
                        <?php if($op=="R"){echo " disabled";} ?> required>
                    <option value="">-- Choix --</option>
                    <?php
                    // On récupère la liste des moyens de paiement de la BDD
                        foreach ($listePaymentMethod as $keyIdmethod => $valuemethod) {	
                        /* Si c'est une modification ou restauration de transaction, alors on sélectionne
                        par défaut le paiement stocké en BDD pour cette transaction */
                            if($op=="M" || $op=="R"){
                                if($valuemethod==$transactionToDisplay['Paiement']){
                                    
                                    echo "<option value=\"$keyIdmethod\" selected>$valuemethod</option>";
                                }
                                else {
                                    echo "<option value=\"$keyIdmethod\">$valuemethod</option>";
                                };}
                            // Si on créé une transaction, alors aucune entrée n'est présélectionnée
                            else {echo "<option value=\"$keyIdmethod\">$valuemethod</option>";}
                        }
                    ?> 
                </select>
                <label>Méthode de paiement</label>
            </div>
        </div>
        <?php 
        /* S'il s'agit d'une création de transaction, on affiche le bouton "Créer". Si c'est une modification, on affiche
        les boutons Modifier et Supprimer */
            if($op=="C"){
                echo '<input type="submit" name="CreerTransaction" class="btn right" value="Creer">';
            } 
            else if ($op=="M"){
                echo '<input type="submit" name="EditTransaction" class="btn right" value="Modifier">';
                echo '<input type="submit" name="DeleteTransaction" class="btn red" value="Supprimer">';
            }
            else if ($op=="R"){
                echo '<input type="submit" name="RestoreTransaction" class="btn red right" value="Restaurer">';
            }
        ?>
    </form>
</div>
<!-- ///////////////////////////////////////// Footer ////////////////////////////////////////////////////////// -->
<?php
include('lib/templates/footerFixed.php');
?>

<!--  Scripts liés au framework Materialize + jQuery -->
<script src="jQuery/jquery-2.1.1.min.js"></script>
<script src="jQuery/jquery-ui.min.js"></script>
<script src="JS/materialize.js"></script>
<!--  Scripts paramétrage jQuery -->
<script src="JS/init.js"></script>

</body>
</html>