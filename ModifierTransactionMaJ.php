<?php
require_once('lib/transactionUpdatedChecked.php');

?>
<!-- ///////////////////////////////////// PAGE HTML //////////////////////////////////////////////////////// -->
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modification transaction</title>
    <!-- CSS  du framework materialize -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="CSS/materialize.css" type="text/css" rel="stylesheet" media="screen"/>
    <!-- Surcouche CSS -->
    <link href="CSS/style.css" type="text/css" rel="stylesheet" media="screen"/>
    <!-- JQuery -->
    <link rel="stylesheet" href="jQuery/jquery-ui.min.css">
</head>
<!-- Au chargement de la page, on ouvre la modal qui informe l'utilisateur du résultat de l'opération -->
<body onload="openModalMajTransac()">
    <nav class="light-blue lighten-1">
        <div class="nav-wrapper container">
            <a id="logo-container" href="#" class="brand-logo">
                <img src="img/LogoMyBudget.png" id="LogoAppli" alt="Logo de l'application MyBudget">
            </a>
            <h1 class="center appli-name hide-on-med-and-down">MyBudget</h1>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <h2 class="center noMarginTop noMarginBottom">Modifier une transaction</h2>
        </div>

        <!-- Lien qui ouvre la modal (activé automatiquement par script JS au chargement de la page) -->
        <a class="waves-effect waves-light btn modal-trigger hide" href="#modalMessage" id="modalTriggerMaJTransac"></a>

        <!-- Modal Structure -->
        <div id="modalMessage" class="modal">
            <div class="modal-content">
            <?php 
            // Si la variable $success existe, alors l'opération a fonctionné, et on affiche le retour
                if(isset($success)){
                    echo "<p>$success</p>";
                } 
            //Si la variable $errorDetect existe, alors on affiche le texte disant qu'une erreur est survenue, mais sans donner le détail (sécurité)
                else if(isset($errorDetect)){
                    echo "<p>Aïe ! Une erreur est survenue...</p>";
                }
            ?>
            </div>
            <div class="modal-footer">
            <a href="List.php" class="modal-close waves-effect waves-green btn">OK</a>
            <?php
            //Si l'opération est une suppression, on affiche un bouton "Restaurer" permettant immédiatement d'annuler la suppression (si l'utilisateur s'est trompé)
                if(isset($successDelete)){
                    echo "<a href='ModifierTransaction.php?idTransaction=$transactionId&Restore=1' class='modal-close waves-effect waves-green btn red'>Annuler la suppression</a>";
                }
            ?>
            </div>
        </div>
    </div>
<!-- ///////////////////////////////////////// Footer ////////////////////////////////////////////////////////// -->
<?php
include('lib/templates/footer.php');
?>

<!--  Scripts liés au framework Materialize + jquey-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="jQuery/DatePicker/jquery-ui.min.js"></script>
<script src="JS/materialize.js"></script>
<!-- Script paramétrage jQuery -->
<script src="JS/init.js"></script>

</body>
</html>