<?php
/* Cette page affiche la page d'accueil de l'application */
require_once("lib/utils.php");
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Accueil MyBudget</title>
    <!-- CSS  du framework materialize -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen"/>
    <!-- Surcouche CSS -->
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen"/>
</head>

<body>
<nav class="light-blue lighten-1">
    <div class="nav-wrapper container">
      <h1 class="center appli-name">MyBudget</h1>
    </div>
</nav>

<div id="index-banner" class="parallax-container">
    <div class="section" id="sectionAccueil">
        <div class="container">
            <div class="row center">
                <a href="List.php" class="btn-large waves-effect waves-light teal lighten-1">Démarrer <i class="material-icons right">send</i></a>
            </div>
        </div>
    </div>
    <div class="parallax">
      <img src="img/ImgAccueil.png" id="imgAccueil" alt="Illustration Budget" title="Une image d'illustration">
  </div>
</div>

<?php
include('lib/templates/footer.php');
?>
<!--  Scripts liés au framework Materialize + jquery -->
<script src="jQuery/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>

</body>
</html>