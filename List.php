<?php

require_once("lib/listScript.php");
?>

<!-- ////////////////////////////////////////////////////// HTML ///////////////////////////////////////////// -->
<!doctype html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Liste des opérations</title>
    <!-- CSS  du framework materialize -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="CSS/materialize.css" type="text/css" rel="stylesheet" media="screen"/>
    <!-- Surcouche CSS -->
    <link href="CSS/style.css" type="text/css" rel="stylesheet" media="screen"/>
    <!-- JQuery DatePicker -->
    <link rel="stylesheet" href="jQuery/jquery-ui.min.css">
</head>

<body class="grey lighten-2">
<nav class="light-blue lighten-1">
    <div class="nav-wrapper container">
        <a id="logo-container" href="index.php" class="brand-logo">
            <img src="img/LogoMyBudget.png" id="LogoAppli" alt="Logo de l'application MyBudget">
        </a>
        <h1 class="center appli-name hide-on-med-and-down">MyBudget</h1>

        <!-- //////////////////////////  Menu Hamburger Mobile ////////////////////  -->
        <ul id="nav-mobile" class="sidenav blue lighten-4">
            <li><a href="#"><b>Liste Opérations</b></a></li>
            <li><a href="Stat.php">Bilan annuel</a></li>
            <li><a href="index.php">Quitter</a></li>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <!-- //////////////////////////  Fin Menu Mobile ////////////////////  -->
    </div>
        
    <a href="Stat.php" class="btn purple lighten-1 right RapportListeButton hide-on-med-and-down">Rapport annuel</a>
    <a href="index.php" class="btn red right QuitterButton">Quitter</a>
</nav>

<!-- /////////////////////////////////////////////////////////  Affichage ///////////////////////////////////////// -->
<div class= "container">
    <div class="row">
        <h2 class="center noMarginTop noMarginBottom">Liste des transactions</h2>
    </div>
    <!-- Bouton Créer Desktop -->
    <div class="row hide-on-med-and-down noMarginBottom">
        <a href="ModifierTransaction.php?idTransaction=0"  class="waves-effect waves-light btn blue lighten-2">Créer</a>
    </div>
    <!-- Bouton Créer float Mobile -->
    <div class="row hide-on-large-only">
        <a href="ModifierTransaction.php?idTransaction=0"  class="btn-floating btn-large waves-effect waves-light red btnAddMobile"><i class="material-icons">add</i></a>
    </div>
    <!-- Bouton filtrer -->
    <div class="row noMarginBottom right">
        <a href="#DefineFiltres"  class="waves-effect waves-light btn modal-trigger orange lighten-2" id="Teeeest">Filtrer</a>
    </div>

    <!-- //////////////////////////////////////////  Modal Sélection filtres ///////////////////////////////////////-->
    <div id="DefineFiltres" class="modal">
        <div class="modal-content">
            <form class="col s12" action="List.php" method="get">
                <!-- Filtres pré-paramétrés, ne s'affichent que en Desktop -->
                <div class="row hide-on-med-and-down">
                    <a href="List.php?filtre=2" class="col m12 l4 waves-effect waves-light btn modal-trigger cyan lighten-2 left">Année</a>
                    <a href="List.php?filtre=3" class="col m12 l4 waves-effect waves-light btn modal-trigger green lighten-2 right">Recettes</a>
                </div>
                <div class="row hide-on-med-and-down">
                    <a href="List.php?filtre=1" class="col m12 l4 waves-effect waves-light btn modal-trigger cyan lighten-2 left">Supprimées</a>
                    <a href="List.php?filtre=4" class="col m12 l4 waves-effect waves-light btn modal-trigger red lighten-2 right">Dépenses</a>
                </div>
                <!-- Menu déroulant des Catégories (requête BDD) -->
                <div class="row">
                    <div class="input-field col s12 l6">
                        <select size="1" name="categoryId">
                            <option value="" disabled selected>-- Choix --</option>
                            <optgroup label="Recettes">
                                <?php
                                // On liste les catégories de recettes récupérées en BDD
                                $catRecette = $catModel->findAllRecettes();
                                foreach ($catRecette as $donnees){
                                        echo "<option value=".$donnees['categoryId'].">".$donnees['name']."</option>";
                                }
                                ?>
                            </optgroup>
                            <optgroup label="Dépenses">
                                <?php
                                // On liste les catégories de dépenses récupérées en BDD
                                $catDepense = $catModel->findAllDepenses();
                                foreach ($catDepense as $donnees){
                                        echo "<option value=".$donnees['categoryId'].">".$donnees['name']."</option>";
                                    }
                                ?>
                            </optgroup>
                        </select>
                        <label>Catégorie</label>
                    </div>
                    <!-- Menu déroulant Méthodes de paiement récupérées en BDD-->
                    <div class="input-field col s12 l6">
                        <select size="1" name="MethodePaymentId">
                            <option value="" disabled selected>-- Choix --</option>
                            <?php
                            //On liste les moyens de paiements récupérés en BDD
                            $payList = $payModel -> findAll();
                            foreach ($payList as $id => $name) {
                                echo "<option value=".$id.">".$name."</option>";
                            }
                            ?>
                        </select>
                        <label>Méthode de paiement</label>
                    </div>
                </div>
                <!-- Montant min et max -->
                <div class="row">
                    <div class="input-field col s12 l6">
                        <input type="number" name="amountMin" min=0 step=0.01 class="validate" id="amountMin">
                        <label for="amountMin">Montant min (€)</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <input type="number" name="amountMax" min=0 step=0.01 class="validate" id="amountMax">
                        <label for="amountMax">Montant max (€)</label>
                    </div>
                </div>
                <!-- Date min et date max -->
                <div class="row">
                    <div class="input-field col s12 l6">
                        <input type="text" name="transactionDateMin" id="dateTransactionMin" class="datepicker" value="<?php echo $monthBegin ?>">
                        <label for="dateTransactionMin">Date début</label>
                    </div>
                    <div class="input-field col s12 l6">
                        <input type="text" name="transactionDateMax" id="dateTransactionMax" class="datepicker" value="<?php echo $monthEnd ?>">
                        <label for="dateTransactionMax">Date fin</label>
                    </div>
                </div>
                <!-- Inclure opérations supprimées ou non -->
                <div class="row">
                    <div class="switch">
                        <label>
                            <input type="checkbox" name="isDelete" value="1">
                            <span class="lever"></span>
                            Inclure les opérations supprimées
                        </label>
                    </div>
                </div>
                <button class="btn">Envoyer</button>
            </form>
        </div>
    </div>
    <!-- /////////////////////////////////////////////////////////  Affichage Filtres ///////////////////////////// -->
    <?php
    // Affichage descriptif des filtres appliqués à la liste (info utilisateur)
    if(($SQLWhere !="`dateSupp`IS NULL" && $nbFiltre != 1) || $filtre!=false){
        echo "<p>Filtres : ".$afficheFiltre."</p>";}
    ?>

    <!-- ///////////////////////////////////////////////  Affichage nb de lignes et cumul ///////////////////////////// -->
    <?php   // On affiche le nombre de lignes du tableau, en incluant le nombre de lignes supprimées si le filtre "inclure les op supprimées" est sélectionné
    if($isDelete==1){
        if($nbLignesPhpSup > 1){
            echo "<p>".$nbLignesPhp." lignes affichées, dont $nbLignesPhpSup supprimées.</p>";
        } else {
            echo "<p>".$nbLignesPhp." lignes affichées, dont $nbLignesPhpSup supprimée.</p>";
        }
    } else {
        if($nbLignesPhp > 1){
            echo "<p>".$nbLignesPhp." lignes affichées.</p>";
        } else {
            echo "<p>".$nbLignesPhp." ligne affichée.</p>";
        }
    }
    // Affichage de la somme des montants des lignes affichées, lignes supprimées inclues
    if($isDelete==1){
        echo "<p>Le total cumulé (lignes supprimées incluses) est de $amountBalance €</p>";
    } else {
        echo "<p class='noMarginBottom'>Le total cumulé est de $amountBalance €</p>";
    }
    ?>
    <!-- ///////////////////////////////////////////////  Affichage Tableau ///////////////////////////// -->
    <table id="operationTable">
        <thead>
            <tr>
                <th>Date/heure</th>
                <th>Montant</th>
                <th>Catégorie</th>
                <!-- La colonne Paiement n'est pas affichée en version Mobile -->
                <th class="hide-on-small-only">Paiement</th>
                <!-- L'en tête de colonne du bouton Modifier/Restaurer n'est pas affiché à l'utilisateur -->
                <th class="hide">Modifier</th>
            </tr>
        </thead>
        <tbody>
            <?php
            // On parcours le résultat de la requête SQL (ligne 157)
            foreach ($listTransactionAsked as $donnees){

                //Si la transaction est une recette, le montant sera vert, sinon rouge
                if($donnees['Type']=="1"){
                    $colorAmount = "green";}
                else{
                    $colorAmount = "red";}

                //Si la transaction est supprimée, elle apparaît en barrée
                if($donnees['dateSupp'] != NULL){
                    $crossLine = "style='text-decoration:line-through;'";}
                else{$crossLine ='';}

                // On transforme le date time MySQL au format lisible par l'utilisateur (au format : "Le 01/01/2021 à 9h00")
                $DateAffichageFront = $convert_Date->dateUserFriendly($donnees['transactionDate']);

                echo "<tr $crossLine>";
                    //Contenu colonne Date/heure
                    echo "<td>".$DateAffichageFront."</td>";
                    //Contenu colonne Montant
                    echo "<td class='$colorAmount-text'>".$donnees['amount']."€</td>";
                    //Contenu colonne Catégorie
                    echo "<td>".$donnees['Catégorie']."</td>";
                    //Contenu colonne Paiement (pas affichée en vue mobile)
                    echo "<td class='hide-on-small-only'>".$donnees['Paiement']."</td>";

                    //Colonne bouton modifier / restaurer
                    //Si la transaction est pas supprimée, on affiche le bouton Restaurer, sinon on affiche le bouton Modifier
                if($donnees['dateSupp'] != NULL){
                    echo '<td><a class="btn red right" href="ModifierTransaction.php?idTransaction='.$donnees['transactionId'].'&Restore=1">
                            <i class="material-icons">restore</i></a></td>';
                }
                else{
                    echo '<td><a class="btn right" href="ModifierTransaction.php?idTransaction='.$donnees['transactionId'].'">
                            <i class="material-icons">edit</i></a></td>';
                }
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>
</div>
<!-- ///////////////////////////////////////////////  Footer ///////////////////////////// -->
<?php
include('lib/templates/footer.php');
?>

<!--  Scripts liés au framework Materialize + jQuery -->
<script src="jQuery/jquery-2.1.1.min.js"></script>
<script src="jQuery/DatePicker/jquery-ui.min.js"></script>
<script src="JS/materialize.js"></script>
<!-- Script de paramétrage jQuery -->
<script src="JS/init.js"></script>

</body>
</html>