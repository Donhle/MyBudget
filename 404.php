<!DOCTYPE html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Page inexistante :(</title>
    <!-- CSS  du framework materialize -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen"/>
    <!-- Surcouche CSS -->
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen"/>
</head>

<body>
<nav class="light-blue lighten-1">
    <div class="nav-wrapper container">
        <h1 class="center appli-name">MyBudget</h1>
    </div>
</nav>
<div class="container center">
    <h2>Page inexistante :(</h2>
    <p>Désolé, cette page n'existe pas.</p>
    <p><a href="list.php">Retour à la liste des transactions</a>

</div>
<?php
include('lib/template/footer.php');
?>

<!--  Scripts liés au framework Materialize-->
<script src="jQuery/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>
</body>
</html>