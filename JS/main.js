//Ce script permet d'activer la bibliothèque jQuery "Datepicker" sur le champ "DateRDV", de le traduire en français, et de restreindre la sélection de date possible

//Activation bibliothèque jQuery de sélection d'une date sur le champ de sélection de la date du RDV

$( function() {
$( "#dateTransactionMin" ).datepicker();
} );

$( function() {
$( "#dateTransactionMax" ).datepicker();
} );

$( function() {
	$( "#dateTransaction" ).datepicker();
	} );

//Paramétrage du Datepicker jQuery

jQuery(function($){
$.datepicker.regional['fr'] = {
	//Traduction en français
	closeText: 'Fermer',
	prevText: '&#x3c;Préc',
	nextText: 'Suiv&#x3e;',
	currentText: 'Aujourd\'hui',
	monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin',
	'Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
	monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun',
	'Jul','Aou','Sep','Oct','Nov','Dec'],
	dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
	dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
	dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
	weekHeader: 'Sm',
	dateFormat: 'yy-mm-dd',
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: '',
	//Paramétrage des dates sélectionnables: La plage va de la date du jour jusque dans un an, on ne montre qu'un mois à la fois
	minDate: '-12M',
	maxDate: '0',
	numberOfMonths: 1,
	showButtonPanel: false,
	defautDate: new Date(Date.now())
	};
$.datepicker.setDefaults($.datepicker.regional['fr']);
	});
