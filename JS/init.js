(function($){
  $(function(){

    $('.sidenav').sidenav();

  }); // end of document ready
})(jQuery); // end of jQuery name space

//Datepicker
  $(document).ready(function(){
  $('.datepicker').datepicker({
        autoClose:true,
        format:"yyyy-mm-dd",
        firstDay:1,
        showDaysInNextAndPreviousMonths:true,
        i18n:{
          cancel:'Annuler',
          months: [ 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre' ],
          monthsShort: [ 'Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec' ],
          weekdays: [ 'Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi' ],
          weekdaysShort: [ 'Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam' ]
        }
      }
  );
});

// Modal materialize

  $(document).ready(function(){
  $('.modal').modal();
});

// Script d'activation des input select (materialize)

  $(document).ready(function() {
  $('select').formSelect();
});

// Script Timepicker materialize
if(document.getElementById('heureTransaction')){
    $(document).ready(function(){
      $('.timepicker').timepicker({
        twelveHour: false,
        defaultTime: document.getElementById('heureTransaction').value
      });
});
}
/*
if(document.getElementById('modalTriggerMaJTransac')){
    let lienModal = document.getElementById('modalTriggerMaJTransac');
    lienModal.click();
}*/


//Datepicker
  $(document).ready(function(){
  $('.datepicker').datepicker({
        autoClose:true,
        format:"yyyy-mm-dd",
        firstDay:1,
        showDaysInNextAndPreviousMonths:true,
        i18n:{
          cancel:'Annuler',
          months: [ 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre' ],
          monthsShort: [ 'Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec' ],
          weekdays: [ 'Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi' ],
          weekdaysShort: [ 'Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam' ]
        }
      }
  );
});

// Script Timepicker materialize
if(document.getElementById('heureTransaction')){

    $(document).ready(function(){
        $('.timepicker').timepicker({
            twelveHour: false,
            defaultTime: document.getElementById('heureTransaction').value
        });
    });
}

// Modal materialize

  $(document).ready(function(){
  $('.modal').modal();
    });

// Ouverture modal au chargement de ModifierTransactionMAJ.php
function openModalMajTransac(){
    document.getElementById('modalTriggerMaJTransac').click();
}

// Script d'activation des input select (materialize)

  $(document).ready(function() {
  $('select').formSelect();
});





